let doWantToPlay = confirm('Do you want to play a game?');
let prizes = [0, 100, 50, 25];
let randomIndex = 9;
let totalPrize = 0;

function getRandomNumber(index) {
    return Math.floor(Math.random() * index);
}

for(let i = 0; i<1; i++){
    if (!doWantToPlay) {
        alert('You did not become a billionaire, but can.');
    } else {
        let randomNumber = getRandomNumber(randomIndex);
        console.log(randomNumber);
        let isSuccessful = false;
        
        for (let j = 0; j < 3; j++) {
            
            let userInput = +prompt(
        `Choose a roulette pocket number from 0 to ${randomIndex - 1}
        Attemps left: ${3-j}
        Total prize: ${totalPrize}
        Possible prize on current attemp: ${prizes[j+1]}`
            );
             
            if (randomNumber === userInput) {
            isSuccessful = true;
            doWantToPlay = confirm(
                `Congratulation, you won! Your prize is: ${prizes[j+1]}$. Do you want to continue?`
            );
            
            totalPrize += prizes[j+1];
            randomIndex += 4;
            prizes = prizes.map(function(element) {
                return element * 2;
            });
            
            console.log(randomIndex);
            break;
            }
        }
        i--;
        if (!isSuccessful) {
            alert(`Thank you for your participation. Your prize is: ${prizes[0]}$`);
            break;
        }
    }
}

