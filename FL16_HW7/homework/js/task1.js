let initialAmount = +prompt('Initial Amount');
if ( !parseInt(initialAmount) || initialAmount < 1000) {
    alert('Invalid input data');
}
let years = +prompt('Number of Years');
if (!parseInt(years) || years < 1){
    alert('Invalid input data');
}
let percentage = +prompt('Percentage of Year');
if(!parseInt(percentage) || percentage >= 100){
    alert('Invalid input data');
}
let monthStatuses = [];
let currentValue = initialAmount; 

for (let i = 0;i < years;i++){
   let newValue = currentValue + currentValue * (percentage/100); 
   let profit = newValue - currentValue;   
   monthStatuses.push(`Initial amount: ${initialAmount}
        Number of years: ${years}
        Percentage of year: ${percentage}
        Total profit: ${profit.toFixed(2)}
        Total amount: ${currentValue.toFixed(2)}`); 
   currentValue = newValue; 
}

let totalProfit = currentValue - initialAmount;
let totalAmount = currentValue;

for (let i =0;i<monthStatuses.length;i++){
    alert(`${i+1} year
    ${monthStatuses[i]}`);
}
