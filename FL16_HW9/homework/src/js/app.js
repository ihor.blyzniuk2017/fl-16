let formContainer = document.querySelector('.form-container');
let inputs = document.querySelectorAll('input');
let nameInput = document.querySelector('#first');
let timeInput = document.querySelector('#second');
let placeInput = document.querySelector('#third');
let submit = document.querySelector('.form-submit');
let converterBtn = document.querySelector('.form-btn');
const regularExpValid = /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/;

function userEventName(){
    let userEvent = prompt('write event name');
    if(userEvent !== ''){
        formContainer.classList.add('active');
    } else {
        userEventName();
    }
}
userEventName();
function validateForm(){
    let name = nameInput.value;
    let time = timeInput.value;
    let place = placeInput.value;
    for(let i = 0; i<1; i++){
        inputs.forEach(item => {
            if(item === ''){
                alert('input all data');
            }
        });
        if(!regularExpValid.test(time)){
            alert('Enter time in format hh:mm');
        } else {
            console.log(`${name} has a meeting today at ${time} somewhere in ${place}`);
        }
    }
        
    
}
submit.addEventListener('click', () => {
    validateForm();
});

function converter(){
    let userDollars = +prompt('dollars');
    let userEuros = +prompt('euros');
    let currencyHryvnaToDollar = 28;
    let currencyHryvnaToEuro = 30;
    let hryvnaToDollar = (userDollars * currencyHryvnaToDollar).toFixed(2);
    let hryvnaToEuro = (userEuros * currencyHryvnaToEuro).toFixed(2);

    alert(`${userEuros} euros are equal ${hryvnaToDollar}hrns, ${userDollars} dollars are equal ${hryvnaToEuro}hrns`);
}

converterBtn.addEventListener('click', () => {
    converter();
});


