function visitLink(path) {
    let counter = localStorage.getItem(path) || 0;
    let nextCountValue = ++counter;

    localStorage.setItem(path, nextCountValue);
}

function viewResults() {
	const arrayOfKeys = ['Page1', 'Page2', 'Page3'];
    const arrayViewResults = arrayOfKeys.map((item) => {
        const localStorageValue = localStorage.getItem(item);
        
        return {
            name: item,
            value: localStorageValue
        };
    });
    const elemBtn = document.querySelector('button');
    
    const elemLi = (item) => `<li>You visited ${item.name} ${item.value} time(s)</li>`;
    const template = `<ul class="list"></ul>`;
    elemBtn.insertAdjacentHTML('afterend', template);
    const ulElem = document.querySelector('.list');
    const resultHtml = arrayViewResults.reduce((acc, elem) => {
        acc += elemLi(elem);
        return acc;
    }, '');
    ulElem.innerHTML = '';
    ulElem.insertAdjacentHTML('afterbegin', resultHtml);
}
