function reverseNumber(num) {
    let result = 0;
    while (num) {
        result = result * 10 + num % 10;
        num = parseInt(num / 10);
    }
    return result;
}

function forEach(arr, func) {
    let array = [];
    for(let i=0;i<arr.length;i++){
        array.push(func(arr[i]));
    }
}


function map(arr, func) {
    let array = [];
    forEach(arr, (el) => {

        array.push(func(el));
    
    });
    return array;
}

function filter(arr, func) {
    let array = [];
    forEach(arr, (el) => {
        if(func(el)) {
            array.push(el);
        }
    });
    return array;
}

let data =[
    {
      '_id': '5b5e3168c6bf40f2c1235cd6',
      'index': 0,
      'age': 39,
      'eyeColor': 'green',
      'name': 'Stein',
      'favoriteFruit': 'apple'
    },
    {
      '_id': '5b5e3168e328c0d72e4f27d8',
      'index': 1,
      'age': 38,
      'eyeColor': 'blue',
      'name': 'Cortez',
      'favoriteFruit': 'strawberry'
    },
    {
        '_id': '5b5e3168cc79132b631c666a',
        'index': 2,
        'age': 2,
        'eyeColor': 'blue',
        'name': 'Suzette',
        'favoriteFruit': 'apple'
      },
      {
        '_id': '5b5e31682093adcc6cd0dde5',
        'index': 3,
        'age': 17,
        'eyeColor': 'green',
        'name': 'Weiss',
        'favoriteFruit': 'banana'
      }
    ];
    
  


function getAdultAppleLovers(data) {
    let array = [];
    forEach(data, (el) => {
        if(el.age >= 18 && el.favoriteFruit === 'apple') {
            array.push(el.name);
        }
    });
    return array;
}

function getKeys(obj) {
    let array = [];
    for (key in obj){
        array.push(key);
    }
        
    
    return array;
}

function getValues(obj) {
    let array = [];
    for (key in obj){
        array.push(obj[key]);
    }
    return array;
}

function showFormattedDate(dateObj) {
    const date = dateObj;
    const dateTimeFormat = new Intl.DateTimeFormat('en', { year: 'numeric', month: 'short', day: '2-digit' }); 
    const [{ value: month },,{ value: day },,{ value: year }] = dateTimeFormat .formatToParts(date );
    return `It is ${day}-${month}-${year }`;
}
