function storeNames(...args){
    let array = [];
    for (let arg of args) {
        array.push(arg);
    }
    return array;
}
function negativeCount(arr){
    let array = [];
    arr.forEach(item => {
        if(item < 0){
            array.push(item);
        }
    });
    return array.length;
}
function getDifference(num1, num2){
    let difference = 0;
    if(num1>=num2){
        difference = num1 - num2;
    } else {
        difference = num2 - num1;
    }
    return difference;
}
function letterCount(str, letter){
    let array = str.split('');
    const countArray = array.filter(item => item === letter);

    return countArray.length;
}

function countPoints(arr){
    return arr.reduce((acc, curr) => {
        const currentMatchPointsArray = curr.split(':');
        const firstTeam = +currentMatchPointsArray[0];
        const secondTeam = +currentMatchPointsArray[1];

        if(firstTeam > secondTeam){
            acc += 3;
        }

        if(firstTeam === secondTeam){
            acc += 1;
        }

        if(firstTeam < secondTeam){
            return acc;
        }

        return acc;
    },0);
}

function isEquals(num1, num2){
    return num1 === num2;
}
function isBigger(num1, num2){
    return num1 > num2;
}